package ru.panasyuk.otus.microservice.authjwt.authservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import ru.panasyuk.otus.microservice.authjwt.authservice.dto.User;

// Configuration class to set up the Redis configuration.
@Configuration
public class RedisConfig {

    // Setting up the Redis template object.
    @Bean
    public RedisTemplate<String, User> redisTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate<String, User> template = new RedisTemplate<>();
        template.setConnectionFactory(connectionFactory);
        // Add some specific configuration here. Key serializers, etc.
        return template;
    }
}