package ru.panasyuk.otus.microservice.authjwt.authservice.service;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import ru.panasyuk.otus.microservice.authjwt.authservice.dao.UserRepo;
import ru.panasyuk.otus.microservice.authjwt.authservice.dto.User;

@Service
public class RedisService implements UserRepo {

    private final String USER_CACHE = "USER";

    @Autowired
    private RedisTemplate<String, User> redisTemplate;
    private HashOperations<String, String, User> hashOperations;

    // This annotation makes sure that the method needs to be executed after
    // dependency injection is done to perform any initialization.
    @PostConstruct
    private void intializeHashOperations() {
        hashOperations = redisTemplate.opsForHash();
    }

    // Save operation.
    @Override
    public void save(final User user) {
        hashOperations.put(USER_CACHE, user.getId(), user);
    }

    // Find by employee id operation.
    @Override
    public User findById(final String id) {
        return (User) hashOperations.get(USER_CACHE, id);
    }

    // Find all employees' operation.
    @Override
    public Map<String, User> findAll() {
        return hashOperations.entries(USER_CACHE);
    }

    // Delete employee by id operation.
    @Override
    public void delete(String id) {
        hashOperations.delete(USER_CACHE, id);
    }
}