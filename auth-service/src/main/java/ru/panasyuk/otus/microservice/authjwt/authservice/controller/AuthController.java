package ru.panasyuk.otus.microservice.authjwt.authservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.panasyuk.otus.microservice.authjwt.authservice.dto.AuthRequest;
import ru.panasyuk.otus.microservice.authjwt.authservice.dto.AuthResponse;
import ru.panasyuk.otus.microservice.authjwt.authservice.dto.UserCreateRequest;
import ru.panasyuk.otus.microservice.authjwt.authservice.dto.UserCreateResponse;
import ru.panasyuk.otus.microservice.authjwt.authservice.service.AuthService;
import ru.panasyuk.otus.microservice.authjwt.authservice.service.JwtUtil;

@RestController
@RequestMapping(value = "/auth")
@RequiredArgsConstructor
public class AuthController {
    private final AuthService authService;
    private final JwtUtil jwt;

    @PostMapping(value = "/register")
    public ResponseEntity<UserCreateResponse> register(@RequestBody UserCreateRequest userCreateRequest) {
        return ResponseEntity.ok(authService.register(userCreateRequest));
    }

    @PostMapping(value = "/login")
    public ResponseEntity<AuthResponse> login(@RequestBody AuthRequest authRequest) {
        return ResponseEntity.ok(authService.authenticate(authRequest));
    }

    @GetMapping(value = "/access")
    public ResponseEntity<String> getAccess(@RequestHeader("Authorization") String header) {
        return ResponseEntity.ok(authService.getAccess(header));
    }

    @GetMapping("/logout")
    public ResponseEntity<String> logout(@RequestHeader("Authorization") String header) {
        final String token = header.replace("Bearer", "").trim();
        String result = authService.getAccess(header);
        jwt.invalidateRelatedTokens(token);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/")
    public ResponseEntity<String> getAll() {
        return ResponseEntity.ok(authService.getAllUsersInCache());
    }
}
