package ru.panasyuk.otus.microservice.authjwt.authservice.service;

import ru.panasyuk.otus.microservice.authjwt.authservice.dto.AuthRequest;
import ru.panasyuk.otus.microservice.authjwt.authservice.dto.AuthResponse;
import ru.panasyuk.otus.microservice.authjwt.authservice.dto.UserCreateRequest;
import ru.panasyuk.otus.microservice.authjwt.authservice.dto.UserCreateResponse;

public interface AuthService {

    UserCreateResponse register(UserCreateRequest userCreateRequest);

    AuthResponse authenticate(AuthRequest authRequest);

    String getAccess(String header);

    String getAllUsersInCache();

}
