package ru.panasyuk.otus.microservice.authjwt.authservice.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.panasyuk.otus.microservice.authjwt.authservice.config.JwtProperties;
import ru.panasyuk.otus.microservice.authjwt.authservice.dto.User;
import ru.panasyuk.otus.microservice.authjwt.authservice.dto.UserCreateResponse;

import javax.annotation.PostConstruct;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class JwtUtil {
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    private final JwtProperties props;
    private Key key;

    @Autowired
    private RedisService redisService;

    @PostConstruct
    public void init() {
        this.key = Keys.hmacShaKeyFor(props.getSecret().getBytes());
    }

    public String getAll() {
        return redisService.findAll().toString();
    }

    public Claims getAllClaimsFromToken(String token) {
        return Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody();
    }

    public Date getExpirationDateFromToken(String token) {
        return getAllClaimsFromToken(token).getExpiration();
    }

    public String generate(UserCreateResponse userCreateResponse, String type) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(FIRST_NAME, userCreateResponse.getFirstName());
        claims.put(LAST_NAME, userCreateResponse.getLastName());
        claims.put("email", userCreateResponse.getEmail());
        User user = new User(
                userCreateResponse.getId(),
                userCreateResponse.getFirstName(),
                userCreateResponse.getLastName(),
                userCreateResponse.getLogin());
        return generateToken(claims, user, type);
    }

    private String generateToken(Map<String, Object> claims, User user, String type) {
        long expirationTimeLong;
        if ("ACCESS".equals(type)) {
            expirationTimeLong = props.getExpiration() * 1000L;
        } else {
            expirationTimeLong = props.getExpiration() * 1000L * 5;
        }
        var createdDate = new Date();
        var expirationDate = new Date(createdDate.getTime() + expirationTimeLong);

        JwtBuilder builder = Jwts.builder()
                .setClaims(claims)
                .setSubject(user.getId())
                .setIssuedAt(createdDate)
                .setExpiration(expirationDate)
                .signWith(key);
        String token = builder.compact();
        redisService.save(user);
        return token;
    }

    public User parseToken(String token){
        var claims  = Jwts.parserBuilder().setSigningKey(key).build()
                .parseClaimsJws(token)
                .getBody();
        if (redisService.findById(claims.getSubject()) != null) {
            return redisService.findById(claims.getSubject());
        }
        return null;
    }

    public void invalidateRelatedTokens(String token) {
        redisService.delete(parseToken(token).getId());
    }

    private boolean isTokenExpired(String token) {
        var expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    @SuppressWarnings("unused")
    public boolean validateToken(String token) {
        return !isTokenExpired(token);
    }

}