package ru.panasyuk.otus.microservice.authjwt.authservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.panasyuk.otus.microservice.authjwt.authservice.dto.*;
import ru.panasyuk.otus.microservice.authjwt.authservice.exception.UnauthorizedException;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthServiceImpl implements AuthService {
    private final RestTemplate restTemplate;
    private final JwtUtil jwt;
    @Value("${spring.web.base-url}")
    private String baseUrl;

    @Override
    public UserCreateResponse register(UserCreateRequest userCreateRequest) {
        log.info("Send request for register to user-service");
        return restTemplate.postForObject(baseUrl + "/user", userCreateRequest, UserCreateResponse.class);
    }

    @Override
    public AuthResponse authenticate(AuthRequest authRequest) {
        try {
            log.info("Send request for auth to user-service");
            ResponseEntity<UserCreateResponse> response = restTemplate.getForEntity(baseUrl + "/user/{login}/{pass}",
                    UserCreateResponse.class, authRequest.getLogin(), authRequest.getPassword());
            UserCreateResponse user = Optional.ofNullable(response.getBody()).orElseThrow();
            String accessToken = jwt.generate(user, "ACCESS");
            String refreshToken = jwt.generate(user, "REFRESH");
            log.info("Get response with token from user-service");
            return new AuthResponse(accessToken, refreshToken);
        } catch (Exception e) {
            log.error("Login or password is failed with exception:" + e.getMessage());
            throw new UnauthorizedException("Login or password is failed", e);
        }
    }

    public String getAccess(String header) {
        try {
            final String token = header.replace("Bearer", "").trim();
            User user = jwt.parseToken(token);
            log.info("Get user from:" + user);
            return " Hello, " + user.getFirstName() + " " + user.getLastName() + "!";
        } catch (Exception e) {
            log.error("Error getting user from token" + e.getMessage());
            throw new UnauthorizedException("Login or password is failed", e);
        }
    }

    @Override
    public String getAllUsersInCache() {
        try {
            log.info("Get all user in cache");
            return jwt.getAll();
        } catch (Exception e) {
            log.error("Error getting user from token" + e.getMessage());
            throw new UnauthorizedException("Login or password is failed", e);
        }
    }

}
