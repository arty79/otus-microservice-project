package ru.panasyuk.otus.microservice.authjwt.authservice.dto;

import java.io.Serializable;

import lombok.*;
import org.springframework.stereotype.Component;

// Employee model class has basic employee-related attributes.
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String firstName;
    private String lastName;
    private String login;
}
