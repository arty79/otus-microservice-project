package ru.panasyuk.otus.microservice.authjwt.authservice.dao;

import ru.panasyuk.otus.microservice.authjwt.authservice.dto.User;

import java.util.Map;

public interface UserRepo {

	// Save a new employee.
	void save(User user);
	
	// Find employee by id.
	User findById(String id);
	
	// Final all employees.
	Map<String, User> findAll();
	
	// Delete a employee.
	void delete(String id);
}
