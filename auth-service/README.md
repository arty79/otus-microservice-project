AUTH SERVICE

docker build -t auth-service:0.0.1 .
docker tag auth-service:0.0.1 arty79/auth-service:0.0.1
docker push arty79/auth-service:0.0.1

http://localhost:9002/swagger-ui.html
http://localhost:9002/actuator/health

helm install otus-auth-chart ./otus-auth-chart --dry-run

minikube addons enable ingress
kubectl get pods -n ingress-nginx | grep ingress
kubectl apply -f ./ingress.yaml

export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services otus-auth)
export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")

echo http://$NODE_IP:$NODE_PORT

otus-auth-chart.default.svc.cluster.local
http://otus-app.default.svc.cluster.local:9001 - baseUrl for claster