LOYALTY

docker build -t loyalty-service:0.0.1 .
docker tag loyalty-service:0.0.1 arty79/loyalty-service:0.0.1
docker push arty79/loyalty-service:0.0.1

http://localhost:9003/swagger-ui.html
http://localhost:9003/actuator/health


kubectl delete all --all
kubectl create namespace app
kubectl config set-context --current --namespace=default
kubectl delete ns app --force --grace-period=0


helm install otus-loyalty-chart ./otus-loyalty-chart --dry-run
helm install redis \
--set password=secretpassword \
stable/redis

export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services otus-app)
export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")

echo http://$NODE_IP:$NODE_PORT