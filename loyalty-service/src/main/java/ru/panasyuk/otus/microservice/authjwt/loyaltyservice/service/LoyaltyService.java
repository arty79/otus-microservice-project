package ru.panasyuk.otus.microservice.authjwt.loyaltyservice.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import ru.panasyuk.otus.microservice.authjwt.loyaltyservice.domain.Card;

import java.util.Collection;
import java.util.Optional;

public interface LoyaltyService {

    Card createCard(Card card);

    Card deleteCard(String cardNumber);

    Optional<Card> getCardByNumber(String number);

    Collection<Card> getCardByUserId(String id);

    Card setCardByUserId(String cardNumber, String userId);

    Collection<Card> getAllCards();

    Long increaseCardBalance(String id, String diff);

    Long decreaseCardBalance(String id, String diff);
}
