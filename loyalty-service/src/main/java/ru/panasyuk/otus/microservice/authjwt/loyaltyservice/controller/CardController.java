package ru.panasyuk.otus.microservice.authjwt.loyaltyservice.controller;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.*;
import ru.panasyuk.otus.microservice.authjwt.loyaltyservice.domain.Card;
import ru.panasyuk.otus.microservice.authjwt.loyaltyservice.dto.CardBalanceChangeRequest;
import ru.panasyuk.otus.microservice.authjwt.loyaltyservice.dto.CardByUserIdRequest;
import ru.panasyuk.otus.microservice.authjwt.loyaltyservice.dto.CardCreateRequest;
import ru.panasyuk.otus.microservice.authjwt.loyaltyservice.dto.CardCreateResponse;
import ru.panasyuk.otus.microservice.authjwt.loyaltyservice.exception.CardNotFoundException;
import ru.panasyuk.otus.microservice.authjwt.loyaltyservice.service.LoyaltyService;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/card")
@RequiredArgsConstructor
public class CardController {
    private final LoyaltyService loyaltyService;

    @GetMapping
    public Collection<CardCreateResponse> getAllCards() {
        return loyaltyService.getAllCards().stream().map(CardCreateResponse::of).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public CardCreateResponse getCard(@PathVariable String id) {

        return CardCreateResponse.of(loyaltyService.getCardByNumber(id).orElseThrow(()
                -> new CardNotFoundException("nfe")));
    }

    @PostMapping("/balance/decrease")
    public Long decreaseCardBalance(@RequestBody CardBalanceChangeRequest balanceChangeRequest) {
        return loyaltyService.decreaseCardBalance(balanceChangeRequest.getCardNumber(), balanceChangeRequest.getAmount());
    }

    @PostMapping("/balance/increase")
    public Long increaseCardBalance(@RequestBody CardBalanceChangeRequest cardByUserIdRequest) {
        return loyaltyService.increaseCardBalance(cardByUserIdRequest.getCardNumber(), cardByUserIdRequest.getAmount());
    }

    @PostMapping("/bind")
    public Card setUsersCard(@RequestBody CardByUserIdRequest cardByUserIdRequest) {
        return loyaltyService.setCardByUserId(cardByUserIdRequest.getCardNumber(), cardByUserIdRequest.getUserId());
    }

    @GetMapping("/user/{userId}")
    public Collection<Card> getUsersCard(@PathVariable String userId) {
        return loyaltyService.getCardByUserId(userId);
    }

    @SneakyThrows
    @PostMapping("/create")
    public CardCreateResponse createCard(@RequestBody CardCreateRequest CardCreateRequest) {
        var card = Card.builder()
                .userId(CardCreateRequest.getUserId())
                .type(CardCreateRequest.getType())
                .number(CardCreateRequest.getNumber())
                .balance(Long.valueOf(CardCreateRequest.getBalance()))
                .status(Boolean.valueOf(CardCreateRequest.getStatus()))
                .build();
        var saved = loyaltyService.createCard(card);
        return CardCreateResponse.of(saved);
    }
    @SneakyThrows
    @PostMapping("/delete/{cardNumber}")
    public CardCreateResponse deleteCard(@PathVariable String cardNumber) {
        var saved = loyaltyService.deleteCard(cardNumber);
        return CardCreateResponse.of(saved);
    }

}
