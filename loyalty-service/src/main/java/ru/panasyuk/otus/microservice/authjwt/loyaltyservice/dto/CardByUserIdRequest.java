package ru.panasyuk.otus.microservice.authjwt.loyaltyservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardByUserIdRequest {
    private String cardNumber;
    private String userId;
}