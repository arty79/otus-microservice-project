package ru.panasyuk.otus.microservice.authjwt.loyaltyservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardCreateRequest {
    private String type;
    private String userId;
    private String number;
    private String balance;
    private String status;
}
