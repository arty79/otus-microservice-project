package ru.panasyuk.otus.microservice.authjwt.loyaltyservice.dto;

public enum EventType {
    CREATE,
    DELETE,
    UPDATE,
    CHANGE_BALANCE
}
