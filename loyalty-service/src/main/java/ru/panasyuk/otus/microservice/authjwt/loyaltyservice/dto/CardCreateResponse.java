package ru.panasyuk.otus.microservice.authjwt.loyaltyservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.panasyuk.otus.microservice.authjwt.loyaltyservice.domain.Card;

@Data
@AllArgsConstructor
public class CardCreateResponse {
    private String id;
    private String type;
    private String userId;
    private String number;
    private String balance;
    private String status;

    public static CardCreateResponse of(Card card) {
        return new CardCreateResponse(card.getId().toString(),
                card.getType(),
                card.getUserId(),
                card.getNumber(),
                card.getBalance().toString(),
                card.getStatus().toString());
    }
}
