package ru.panasyuk.otus.microservice.authjwt.loyaltyservice.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFutureCallback;
import ru.panasyuk.otus.microservice.authjwt.loyaltyservice.domain.Card;
import ru.panasyuk.otus.microservice.authjwt.loyaltyservice.dto.EventType;
import ru.panasyuk.otus.microservice.authjwt.loyaltyservice.repository.CardRepository;
import ru.panasyuk.otus.microservice.authjwt.loyaltyservice.service.LoyaltyService;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class LoyaltyServiceImpl implements LoyaltyService {
    private final CardRepository cardRepository;
    private final KafkaTemplate<String, String> kafkaTemplate;
    @Value("${spring.kafka.header-key}")
    private String headerKey;
    @Value("${spring.kafka.eventhub.topic.loyalty-events}")
    private String topicName;

    @Override
    public Card createCard(Card card) {
        log.info("Create card by request: " + card);
        Card savedCard = cardRepository.save(card);
        sendEvent(savedCard, EventType.CREATE);
        return savedCard;
    }

    @Override
    public Card deleteCard(String cardNumber) {
        log.info("Delete card by request: " + cardNumber);
        Optional<Card> result  = cardRepository.findByNumber(cardNumber);
        Card card = null;
        if (result.isPresent()) {
            card = result.get();
            card.setStatus(false);
        }
        Card savedCard = cardRepository.save(card);
        sendEvent(savedCard, EventType.DELETE);
        return savedCard;
    }

    @Override
    public Optional<Card> getCardByNumber(String number) {
        Optional<Card> result = cardRepository.findByNumber(number);
        if (result.isPresent()) {
            log.info("Get card by number: ", number);
        }
        return result.isPresent() ? result : Optional.empty();
    }

    @Override
    public Collection<Card> getCardByUserId(String userId) {
        log.info("Get card by ByUserId: " + userId);
        Collection<Card> result = cardRepository.findByUserId(userId);
        return result.isEmpty() ? Collections.EMPTY_LIST : result;
    }

    @Override
    public Card setCardByUserId(String cardNumber, String userId) {
        log.info("Set card " + cardNumber + " by with UserId: " + userId);
        Optional<Card> result = cardRepository.findByNumber(cardNumber);
        if (result.isPresent()) {
            Card card = result.get();
            card.setUserId(userId);
            cardRepository.save(card);
            sendEvent(card, EventType.UPDATE);
            return card;
        }
        return null;
    }

    @Override
    public Collection<Card> getAllCards() {
        log.info("Get all Cards by request");
        return cardRepository.findAll();
    }

    @Override
    public Long increaseCardBalance(String number, String diff) {
        Long amount = Long.valueOf(diff);
        Optional<Card> result = cardRepository.findByNumber(number);
        if (result.isPresent()) {
            Card card = result.get();
            card.setBalance(card.getBalance() + amount);
            cardRepository.save(card);
            log.info("increase card with number: " + number + "balance on" + amount);
            log.info("card with number: " + number + "have balance " + card.getBalance());
            sendEvent(card, EventType.CHANGE_BALANCE);
            return card.getBalance();
        }
        return 0L;
    }

    @Override
    public Long decreaseCardBalance(String number, String diff) {
        Optional<Card> result = cardRepository.findByNumber(number);
        Long amount = Long.valueOf(diff);
        if (result.isPresent()) {
            Card card = result.get();
            card.setBalance(card.getBalance() - amount);
            cardRepository.save(card);
            log.info("decrease card with number: " + number + "balance on" + amount);
            log.info("card with number: " + number + "have balance " + card.getBalance());
            sendEvent(card, EventType.CHANGE_BALANCE);
            return card.getBalance();
        }
        return 0L;
    }

    private void sendEvent(Card card, EventType eventType) {
        final var initialHeaders = new HashMap<String, Object>();
        initialHeaders.put(headerKey, card.getId());
        initialHeaders.put(KafkaHeaders.TOPIC, topicName + eventType);
        final String requestAsJson;
        try {
            requestAsJson = new ObjectMapper().writeValueAsString(card);
            final var message = new GenericMessage<>(requestAsJson, initialHeaders);
            kafkaTemplate.send(message).addCallback(new ListenableFutureCallback<>() {
                @Override
                public void onFailure(Throwable ex) {
                    log.error("Failed to send message {} to kafka topic {} {}", message.getPayload(), topicName, ex.getMessage());
                }

                @Override
                public void onSuccess(SendResult<String, String> result) {
                    log.info("it was sent to kafka topic {} as a message {}",
                            topicName, message.getPayload());
                }
            });
        } catch (JsonProcessingException e) {
            log.error("Failed to parse message to kafka topic");
        }
    }
}
