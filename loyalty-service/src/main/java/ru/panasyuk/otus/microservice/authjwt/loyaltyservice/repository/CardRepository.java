package ru.panasyuk.otus.microservice.authjwt.loyaltyservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.panasyuk.otus.microservice.authjwt.loyaltyservice.domain.Card;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface CardRepository extends JpaRepository<Card, String> {
    Collection<Card> findByUserId(String userId);
    Optional<Card> findByNumber(String number);
}
