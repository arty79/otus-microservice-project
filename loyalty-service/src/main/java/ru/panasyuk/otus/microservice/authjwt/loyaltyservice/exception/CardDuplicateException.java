package ru.panasyuk.otus.microservice.authjwt.loyaltyservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CardDuplicateException extends RuntimeException {
    public CardDuplicateException(String message) {
        super(message);
    }
}
