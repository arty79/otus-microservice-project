NOTIFY

docker build -t notify-service:0.0.1 .
docker tag notify-service:0.0.1 arty79/notify-service:0.0.1
docker push arty79/notify-service:0.0.1

http://localhost:9004/swagger-ui.html
http://localhost:9004/actuator/health


kubectl delete all --all
kubectl create namespace app
kubectl config set-context --current --namespace=default
kubectl delete ns app --force --grace-period=0


helm install otus-notify-chart ./otus-notify-chart --dry-run

export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services otus-app)
export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")

echo http://$NODE_IP:$NODE_PORT