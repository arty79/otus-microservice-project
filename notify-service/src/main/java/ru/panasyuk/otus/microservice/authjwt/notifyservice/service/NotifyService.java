package ru.panasyuk.otus.microservice.authjwt.notifyservice.service;

import ru.panasyuk.otus.microservice.authjwt.notifyservice.domain.Event;
import ru.panasyuk.otus.microservice.authjwt.notifyservice.dto.EventType;

import java.util.Collection;

public interface NotifyService {

    Collection<Event> getAllEvents();

    Collection<Event> getEventsByType(EventType type);

}
