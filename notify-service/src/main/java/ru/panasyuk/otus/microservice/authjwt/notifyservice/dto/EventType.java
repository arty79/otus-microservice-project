package ru.panasyuk.otus.microservice.authjwt.notifyservice.dto;

public enum EventType {
    CREATE,
    DELETE,
    UPDATE,
    CHANGE_BALANCE
}
