package ru.panasyuk.otus.microservice.authjwt.notifyservice.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.*;

import java.time.LocalDateTime;

/**
 * Базовый класс для документов коллекций, для которых интересно знать время создания и время последнего изменения
 * (проставляются автоматически).
 *
 * @author Андрей Колинко (aa.kolinko@mts.ru)
 */
@Getter
@Setter
@ToString
public class AbstractAuditableDocument {
    @Id
    private String id;

    @CreatedDate
    private LocalDateTime createdTime;

    @LastModifiedDate
    private LocalDateTime lastModifiedTime;
}
