package ru.panasyuk.otus.microservice.authjwt.notifyservice.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Card {
    private Long id;
    private String type;
    private String userId;
    private String number;
    private Long balance;
    private Boolean status;
}
