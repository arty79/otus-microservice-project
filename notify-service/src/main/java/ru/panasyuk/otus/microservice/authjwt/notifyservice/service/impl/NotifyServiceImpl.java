package ru.panasyuk.otus.microservice.authjwt.notifyservice.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.panasyuk.otus.microservice.authjwt.notifyservice.domain.Event;
import ru.panasyuk.otus.microservice.authjwt.notifyservice.dto.EventType;
import ru.panasyuk.otus.microservice.authjwt.notifyservice.repository.EventRepository;
import ru.panasyuk.otus.microservice.authjwt.notifyservice.service.NotifyService;

import java.util.Collection;

@Slf4j
@Service
@RequiredArgsConstructor
public class NotifyServiceImpl implements NotifyService {
    private final EventRepository eventRepository;

    @Override
    public Collection<Event> getAllEvents() {
        log.info("Get all Cards by request");
        return eventRepository.findAll();
    }

    @Override
    public Collection<Event> getEventsByType(EventType type) {
        log.info("Create card by request: " + type);
        return eventRepository.findEventByEventType(type);
    }
}
