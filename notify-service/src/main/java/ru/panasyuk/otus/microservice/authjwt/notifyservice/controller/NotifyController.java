package ru.panasyuk.otus.microservice.authjwt.notifyservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.panasyuk.otus.microservice.authjwt.notifyservice.domain.Event;
import ru.panasyuk.otus.microservice.authjwt.notifyservice.dto.EventType;
import ru.panasyuk.otus.microservice.authjwt.notifyservice.service.NotifyService;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/notify")
@RequiredArgsConstructor
public class NotifyController {
    private final NotifyService notifyService;

    @GetMapping
    public Collection<Event> getAllEvents() {
        return notifyService.getAllEvents().stream().collect(Collectors.toList());
    }

    @GetMapping("/{type}")
    public Collection<Event> getEventByType(@PathVariable String type) {
        return notifyService.getEventsByType(EventType.valueOf(type));
    }
}
