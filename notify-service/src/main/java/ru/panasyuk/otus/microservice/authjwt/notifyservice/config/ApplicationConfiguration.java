package ru.panasyuk.otus.microservice.authjwt.notifyservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

/**
 * Конфигурация spring-context сервиса (misc).
 *
 * @author Андрей Колинко (aa.kolinko@mts.ru)
 */
@Configuration
public class ApplicationConfiguration {

    @Bean
    MappingMongoConverter mappingMongoConverter(MongoDatabaseFactory mongoDatabaseFactory,
                                                MongoMappingContext mongoMappingContext) {

        mongoMappingContext.setAutoIndexCreation(true);

        DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDatabaseFactory);
        MappingMongoConverter converter = new MappingMongoConverter(dbRefResolver, mongoMappingContext);
        // prevent _class field in the collection documents (document inheritance is not supported now)
        converter.setTypeMapper(new DefaultMongoTypeMapper(null));

        return converter;
    }
}
