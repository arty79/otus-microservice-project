package ru.panasyuk.otus.microservice.authjwt.notifyservice.domain;

import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.data.mongodb.core.mapping.Document;
import ru.panasyuk.otus.microservice.authjwt.notifyservice.dto.EventType;


@Getter
@Setter
@Accessors
@ToString
@Document(collection = "events")
public class Event extends AbstractAuditableDocument {
    private Boolean status;
    private Card card;
    private EventType eventType;
}
