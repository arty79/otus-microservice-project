package ru.panasyuk.otus.microservice.authjwt.notifyservice.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.panasyuk.otus.microservice.authjwt.notifyservice.domain.Event;
import ru.panasyuk.otus.microservice.authjwt.notifyservice.dto.EventType;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface EventRepository extends MongoRepository<Event, ObjectId> {
    Collection<Event> findEventByEventType(EventType type);
}
