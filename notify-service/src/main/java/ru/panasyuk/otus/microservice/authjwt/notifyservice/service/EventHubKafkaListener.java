package ru.panasyuk.otus.microservice.authjwt.notifyservice.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import ru.panasyuk.otus.microservice.authjwt.notifyservice.domain.Card;
import ru.panasyuk.otus.microservice.authjwt.notifyservice.domain.Event;
import ru.panasyuk.otus.microservice.authjwt.notifyservice.dto.EventType;
import ru.panasyuk.otus.microservice.authjwt.notifyservice.repository.EventRepository;

/**
 * Listener для очереди event-hub
 *
 * @author Вадим Курбатов (vvkurba1@mts.ru)
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class EventHubKafkaListener {

    private static final String COMMON_TOPIC_NAME = "LOYALTY_EVENTS";

    private static final String RECEIVED_MESSAGE_IN_TOPIC = "Received message in topic {}: {}";

    private final Environment environment;

    private final EventRepository eventRepository;

    @KafkaListener(topics = "LOYALTY_EVENTS_CREATE", concurrency = "2", clientIdPrefix = "notify-service")
    public void processCreateMessage(Message<String> message) throws JsonProcessingException {
        readMessageAndSave(message, EventType.CREATE);
    }

    @KafkaListener(topics = "LOYALTY_EVENTS_DELETE", concurrency = "2", clientIdPrefix = "notify-service")
    public void processDeleteMessage(Message<String> message) throws JsonProcessingException {
        readMessageAndSave(message, EventType.DELETE);
    }

    @KafkaListener(topics = "LOYALTY_EVENTS_UPDATE", concurrency = "2", clientIdPrefix = "notify-service")
    public void processUpdateMessage(Message<String> message) throws JsonProcessingException {
        readMessageAndSave(message, EventType.UPDATE);
    }

    @KafkaListener(topics = "LOYALTY_EVENTS_CHANGE_BALANCE", concurrency = "2", clientIdPrefix = "notify-service")
    public void processChangeMessage(Message<String> message) throws JsonProcessingException {
        readMessageAndSave(message, EventType.CHANGE_BALANCE);
    }

    private void readMessageAndSave(Message<String> message, EventType eventType) throws JsonProcessingException {
        log.info(RECEIVED_MESSAGE_IN_TOPIC, environment.getProperty(COMMON_TOPIC_NAME), message);
        final var card = new ObjectMapper().readValue(message.getPayload(), Card.class);
        Event event = new Event();
        event.setCard(card);
        event.setEventType(eventType);
        eventRepository.save(event);
        log.info("read mesage : " + card);
    }
}