package ru.panasyuk.otus.microservice.authjwt.userservice.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.panasyuk.otus.microservice.authjwt.userservice.domain.User;
import ru.panasyuk.otus.microservice.authjwt.userservice.dto.CardCreateRequest;
import ru.panasyuk.otus.microservice.authjwt.userservice.dto.CardCreateResponse;
import ru.panasyuk.otus.microservice.authjwt.userservice.exception.UserNotFoundException;
import ru.panasyuk.otus.microservice.authjwt.userservice.repository.UserRepository;
import ru.panasyuk.otus.microservice.authjwt.userservice.service.UserService;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {
    private final RestTemplate restTemplate;
    @Value("${spring.web.base-url}")
    private String baseUrl;
    private final UserRepository userRepository;

    @Override
    public Optional<User> getUserByLogin(String login) {
        log.info("Get request for get user by login");
        return userRepository.getUserByLogin(login);
    }

    @Override
    public User getUserByLogin(String login, String plainPass) {
        log.info("Get request for get user by login and password");
        Optional<User> userByLogin = userRepository.getUserByLogin(login);
        if (userByLogin.isEmpty() || !passwordIsCorrect(plainPass, userByLogin.get()))
            throw new UserNotFoundException("user not found: " + login);
        return userByLogin.get();
    }

    @Override
    public Collection<User> getAllUsers() {
        log.info("Get request for get all users");
        return userRepository.findAll();
    }

    @Override
    public User saveUser(User user) {
        log.info("Get request for create user");
        String encoded = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt());
        user.setPassword(encoded);
        User newUser = userRepository.save(user);
        CardCreateRequest cardCreateRequest = new CardCreateRequest();
        cardCreateRequest.setUserId(newUser.getId().toString());
        cardCreateRequest.setType("loyalty");
        cardCreateRequest.setNumber(UUID.randomUUID().toString());
        cardCreateRequest.setBalance("1000");
        cardCreateRequest.setStatus("true");
        CardCreateResponse cardCreateResponse = null;
        try {
            cardCreateResponse =
                    restTemplate.postForObject(baseUrl + "/card/create", cardCreateRequest, CardCreateResponse.class);
        } catch (Exception e) {
            log.error("Something gone wrong..." + e.getMessage());
        } finally {
            log.info("Create user:" + user.getLogin() + " loyalty card: " + cardCreateResponse);
            newUser.setCardCreateResponse(cardCreateResponse);
            return userRepository.save(newUser);
        }
    }

    private boolean passwordIsCorrect(String plainPass, User user) {
        return BCrypt.checkpw(plainPass, user.getPassword());
    }

}
