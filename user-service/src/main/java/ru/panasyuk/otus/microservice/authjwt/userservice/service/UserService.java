package ru.panasyuk.otus.microservice.authjwt.userservice.service;

import ru.panasyuk.otus.microservice.authjwt.userservice.domain.User;

import java.util.Collection;
import java.util.Optional;

public interface UserService {

    Optional<User> getUserByLogin(String login);

    User getUserByLogin(String login, String plainPass);

    Collection<User> getAllUsers();

    User saveUser(User user);



}
