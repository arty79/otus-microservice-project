package ru.panasyuk.otus.microservice.authjwt.userservice.controller;

import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.*;
import ru.panasyuk.otus.microservice.authjwt.userservice.domain.User;
import ru.panasyuk.otus.microservice.authjwt.userservice.dto.UserCreateRequest;
import ru.panasyuk.otus.microservice.authjwt.userservice.dto.UserResponse;
import ru.panasyuk.otus.microservice.authjwt.userservice.exception.AccessDeniedException;
import ru.panasyuk.otus.microservice.authjwt.userservice.exception.UserLoginDuplicateException;
import ru.panasyuk.otus.microservice.authjwt.userservice.exception.UserNotFoundException;
import ru.panasyuk.otus.microservice.authjwt.userservice.service.UserService;

import java.util.Collection;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping
    public Collection<UserResponse> getAllUsers() {
        return userService.getAllUsers().stream().map(UserResponse::of).collect(Collectors.toList());
    }

    @GetMapping("/{login}")
    public UserResponse getUser(@PathVariable String login, @RequestHeader("X-subject") String subject) {
        if (!login.equals(subject)) {
            throw new AccessDeniedException("Access denied");
        }
        return UserResponse.of(userService.getUserByLogin(login).orElseThrow(() -> new UserNotFoundException("nfe")));
    }

    @GetMapping("/{login}/{plainPass}")
    public UserResponse getAllUsers(@PathVariable String login, @PathVariable String plainPass) {
        return UserResponse.of(userService.getUserByLogin(login, plainPass));
    }


    @PostMapping
    public UserResponse createUser(@RequestBody UserCreateRequest userCreateRequest) {
        if (!userService.getUserByLogin(userCreateRequest.getLogin()).isEmpty()) {
            throw new UserLoginDuplicateException("Login already exist!");
        }
        var user = User.builder()
                .firstName(userCreateRequest.getFirstName())
                .lastName(userCreateRequest.getLastName())
                .email(userCreateRequest.getEmail())
                .login(userCreateRequest.getLogin())
                .password(userCreateRequest.getPassword())
                .build();
        var saved = userService.saveUser(user);
        return UserResponse.of(saved);
    }
}
