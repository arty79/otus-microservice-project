USER SERVICE

docker build -t user-service:0.0.1 .
docker tag user-service:0.0.1 arty79/user-service:0.0.1
docker push arty79/user-service:0.0.1

http://localhost:9001/swagger-ui.html
http://localhost:9001/actuator/health

helm install otus-app-chart ./otus-app-chart --dry-run

helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm install mongodb \ bitnami/mongodb --set mongodbRootPassword=pass,mongodbUsername=user,mongodbPassword=pass123

export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services otus-app)
export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")

echo http://$NODE_IP:$NODE_PORT

http://otus-app.default.svc.cluster.local:9003 - baseUrl for cluster

