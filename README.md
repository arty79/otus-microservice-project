**Проект: Система управления лояльностью клиентов**

**Стек технологий**

**Backend:** Java 11, SpringBoot 2.4.5, SpringData, Spring Actuator, JWT, Spring Cloud Gateway

**OpenAPI:** Swagger2

**MessageBroker:** Kafka

**RDBMS:** Postgres SQL

**NoSql:** MongoDB, Redis

**Infrastructure:** Helm + k8s

![img.png](img.png)


**Подготовка к запуску**

`minikube start --cpus=4 --memory=12g`

`minikube addons enable dashboard`

`minikube addons enable ingress`

`minikube dashboard --url`

Все микросервисы запускаются из своих папок при помощи команды:

`skaffold run`

Исключение - установка кафки и монго

**Установка прометея и графаны**

`kubectl create namespace monitor`

`kubectl config set-context --current --namespace=default`

Команда для установки prometheus
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install prom prometheus-community/kube-prometheus-stack -f prometheus.yaml --atomic

Команда для проброса портов для grafana
kubectl port-forward service/prom-grafana 9000:80
Команда для проброса портов для prometeus
kubectl port-forward service/prom-kube-prometheus-stack-prometheus 9999:9090
Grafana credentials
user: admin pas: prom-operator

**Установка кафки** (команды выполняем из папки /kafka)

`kubectl create namespace kafka`

`kubectl config set-context --current --namespace=kafka`

`helm install otus-kafka -f values.yaml bitnami/kafka`

**Установка Монго**

`kubectl apply -f mongodb-secret.yaml`

`kubectl apply -f mongo-configmap.yaml`

`kubectl apply -f deployment-mongo.yaml`

`minikube service mongo-express-service` - откроется Web UI для монго

[дашборд кубернейтс](http://127.0.0.1:51499/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/
)

[коллекция OpenAPI](https://documenter.getpostman.com/view/1788301/UVC8E77d)

`minikube service --url otus-gw` - адрес для API Gateway

`minikube service --url otus-loyalty-chart-postgresql` - адрес для подключения к постгрес
