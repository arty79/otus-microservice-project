GATEWAY SERVICE

docker build -t api-gateway:0.0.1 .
docker tag api-gateway:0.0.1 arty79/api-gateway:0.0.1
docker push arty79/api-gateway:0.0.1

http://localhost:9090/actuator/health

helm install otus-gw-chart ./otus-gw-chart --dry-run

export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services otus-gw)
export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
echo http://$NODE_IP:$NODE_PORT

