package ru.panasyuk.otus.microservice.authjwt.gateway.config;

import lombok.RequiredArgsConstructor;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class GatewayConfig {
    private final AuthenticationFilter filter;

    @Bean
    public RouteLocator routes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("user-service", r -> r.path("/user/**")
                        .filters(f -> f.filter(filter))
                        .uri("http://otus-app.default.svc.cluster.local:9001"))
                .route("auth-service", r -> r.path("/auth/**")
                        .filters(f -> f.filter(filter))
                        .uri("http://otus-auth.default.svc.cluster.local:9002"))
                .route("loyalty-service", r -> r.path("/card/**")
                        .filters(f -> f.filter(filter))
                        .uri("http://otus-loyalty.default.svc.cluster.local:9003"))
                .route("notify-service", r -> r.path("/notify/**")
                        .filters(f -> f.filter(filter))
                        .uri("http://otus-notify.default.svc.cluster.local:9004"))
                .build();
    }

}
